import json
import time
import os
import sys
from airflow.hooks.http_hook import HttpHook
from airflow.contrib.hooks.wasb_hook import WasbHook
from tempfile import NamedTemporaryFile
import pandas as pd

# inside another PythonOperator where provide_context=True
def ffr_tenant2arreas_to_wasb(ffr_token,
                              ffr_conn_id,
                              dest_wasb_conn_id,
                              dest_url,
                              file_name,
                              **kwargs):
  access_token = ffr_token
  ffr_hook = HttpHook(http_conn_id=ffr_conn_id, method='GET')
  ffr_rsp = ffr_hook.run(
    endpoint=f"/api/retail-analytics/tenants?access_token={access_token}",
  )
  if ffr_rsp:
    rsp_json = ffr_rsp.json()
    wasb_hook = WasbHook(wasb_conn_id=dest_wasb_conn_id)

    # create and pass the path
    file_object = rsp_json
    with NamedTemporaryFile(mode='w', delete=True) as f:
      json.dump(file_object, f)
      f.flush()

      # upload file to destination
      az_container_name = f"{dest_url}"
      az_blob_name = "tenant_mappings.json"

      # file to upload from local path
      local_path = os.path.expanduser("/tmp/")
      local_file_name = f.name
      full_path_to_file = os.path.join(local_path, local_file_name)

      # Upload file to azure blob storage
      wasb_hook.load_file(full_path_to_file, az_container_name, az_blob_name)
  return dest_url + file_name

def ffr_user_data_to_wasb(ffr_token: str,
                          ffr_conn_id: str,
                          collection_name: str,
                          interaction_field: str,
                          items_field: str,
                          dest_wasb_conn_id,
                          business_location_id: str,
                          min_item_interactions: str,
                          data_wasb_obj,
                          data_field='aditionalData',
                          user_id_field='clientId',
                          item_id_field='_id',
                          **kwargs):
  wasb_hook = WasbHook(wasb_conn_id=dest_wasb_conn_id)

  ffr_hook = HttpHook(http_conn_id=ffr_conn_id, method='GET')
  fetch_url = f"/api/temp/{collection_name}"
  params = {
    "itemName": items_field,
    "minItemInteractions": 2,
    "access_token": ffr_token
  }

  if items_field == 'tenants':
    params["tenantLocationId"] = business_location_id

  users = 1
  limit = 1000
  params["skip"] = 0
  data = []
  while users:
    rsp = ffr_hook.run(
      endpoint=fetch_url,
      data=params
    )
    if rsp:
      users = rsp.json()
      for document in users:
        df = pd.DataFrame(document[data_field][items_field])
        # filter by interaction field
        df = df[df[interaction_field] > 0] if interaction_field != 'totalSpent/totalReceipts' else df[df['totalReceipts'] > 0]
        # todo: data generation bug, return tenants outside the corresponding location id
        #  even the filtration by loc id is correct
        if 'locationId' in df:
          df = df[df['locationId'] == business_location_id]
        # todo: bad logic from data source provider
        # filter for minItemInteractions
        if df.shape[0] < min_item_interactions:
          continue

        # add interactions
        data += [{
          'users': str(document[user_id_field]),
          items_field: str(row[item_id_field]),
          interaction_field: row[interaction_field] if interaction_field != 'totalSpent/totalReceipts'
          else row['totalSpent'] / row['totalReceipts']
        } for _, row in df.iterrows()]
    params["skip"] += limit
    time.sleep(0.1)

  if data:
    # create and pass the path
    with NamedTemporaryFile(mode='w', delete=True) as f:
      # json.dump(data, f)
      pd.DataFrame(data).to_csv(f, index=False)
      f.flush()

      # upload the file to destination
      az_container_name = f"{data_wasb_obj}"
      az_blob_name = items_field + ".csv"

      # file to upload from local path
      local_path = os.path.expanduser("/tmp/")
      local_file_name = str(f.name)
      full_path_to_file = os.path.join(local_path, local_file_name)

      # Upload file to azure blob storage
      wasb_hook.load_file(full_path_to_file, az_container_name, az_blob_name)
