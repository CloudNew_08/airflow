import json
from datetime import timedelta
from typing import Any, Union

from airflow import DAG
from airflow.models import Variable
from ffr_plugin import RetailAnalyticsVisitsToWasbOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.http_operator import SimpleHttpOperator
from airflow.utils.dates import days_ago, parse_execution_date

from ffr_ops.ffr_data import ffr_tenant2arreas_to_wasb, ffr_user_data_to_wasb

datasets_cfg = Variable.get('datasets_conf', deserialize_json=True)


def pull_access_token(task_id: str,
                      token_key='access_token',
                      **kwargs):
    token_rsp = kwargs['task_instance'].xcom_pull(task_ids=task_id)
    access_token = json.loads(token_rsp)[token_key]
    return access_token


def create_dag(dag_id,
               start_date,
               schedule_interval,
               default_args,
               container_name,
               cloud_storage_conn_id,
               _ffr_conn_id,
               ra_gateway_conn_id,
               business_client,
               location_id,
               ra_building_conn_id,
               interaction_type,
               interaction_field,
               filename,
               file_ext):
    dag = DAG(
        dag_id,
        # start_date=start_date,
        default_args=default_args,
        # description='E',
        schedule_interval=schedule_interval,
    )
    with dag:
        # connect to FFR get token
        t1_ffr_token = SimpleHttpOperator(
            task_id='t1_ffr_token',
            http_conn_id=_ffr_conn_id,
            endpoint='/api/token',
            data={
                'grant_type': 'client_credentials'
            },
            headers={"Content-Type": "application/x-www-form-urlencoded"},
            response_check=lambda response: 'access_token' in response.json(),
            xcom_push=True,
        )

        # pull ffr token response, extract access token and pull again
        t2_pull_ffr_token = PythonOperator(
            task_id='t2_pull_ffr_token',
            python_callable=pull_access_token,
            op_kwargs={
                'task_id': 't1_ffr_token',
            },
            provide_context=True,
        )

        if interaction_type == "visits":
            print("interaction_type visits :", interaction_type)
            # upload tenant-area mappings to wasb
            t3_ffr_tenant_map_to_wasb = PythonOperator(
                task_id='t3_ffr_tenant_map_to_wasb',
                python_callable=ffr_tenant2arreas_to_wasb,
                op_kwargs={
                    'ffr_token': '{{ task_instance.xcom_pull(task_ids="t2_pull_ffr_token") }}',
                    'ffr_conn_id': _ffr_conn_id,
                    'dest_wasb_conn_id': cloud_storage_conn_id,
                    'dest_url': f"{container_name}/{business_client}",
                    'delegate_to': None,
                    'file_name': 'tenant_mappings.json'
                },
                provide_context=True
            )

            t4_ra_token = SimpleHttpOperator(
                task_id='t4_ra_token',
                http_conn_id=ra_gateway_conn_id,
                endpoint='/token',
                data={
                    'grant_type': 'client_credentials'
                },
                headers={"Content-Type": "application/x-www-form-urlencoded"},
                response_check=lambda response: 'access_token' in response.json(),
                xcom_push=True
            )

            # pull ffr token response, extract access token and pull again
            t5_pull_ra_token = PythonOperator(
                task_id='t5_pull_ra_token',
                python_callable=pull_access_token,
                op_kwargs={
                    'task_id': 't4_ra_token',
                },
                provide_context=True
            )

            t6_rav_to_wasb = RetailAnalyticsVisitsToWasbOperator(
                task_id='t6_rav_to_wasb',
                ra_gateway_token='{{ task_instance.xcom_pull(task_ids="t5_pull_ra_token") }}',
                ra_building_conn_id=ra_building_conn_id,
                wasb_conn_id=cloud_storage_conn_id,

                # todo: parametrize this, maybe since: now - 30 days, until: now
                since='2020-10-01T00:00:00',
                until='2020-10-31T00:00:00',

                tenant_map_wasb_obj='{{ task_instance.xcom_pull(task_ids="t3_ffr_tenant_map_to_wasb") }}',
                visits_wasb_obj=f"{container_name}/{business_client}/{location_id}/{interaction_type}",
                provide_context=True
            )

            t1_ffr_token >> t2_pull_ffr_token >> t3_ffr_tenant_map_to_wasb >> t4_ra_token >> t5_pull_ra_token >> t6_rav_to_wasb
        else:
            # upload tenant-area mappings to wasb
            t3_ffr_user_data_to_wasb = PythonOperator(
                task_id='t3_ffr_user_data_to_wasb',
                python_callable=ffr_user_data_to_wasb,
                op_kwargs={
                    'ffr_token': '{{ task_instance.xcom_pull(task_ids="t2_pull_ffr_token") }}',
                    'ffr_conn_id': _ffr_conn_id,
                    'collection_name': 'online-clients',  # todo: talk about this
                    'interaction_field': interaction_field,
                    'items_field': filename,
                    'business_location_id': location_id,
                    'min_item_interactions': 2,
                    'dest_wasb_conn_id': cloud_storage_conn_id,
                    'data_wasb_obj': f"{container_name}/{business_client}/{location_id}/{interaction_type}",
                },
                provide_context=True
            )
            t1_ffr_token >> t2_pull_ffr_token >> t3_ffr_user_data_to_wasb

    return dag


# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
_default_args = {
    'owner': 'airflow',
    # 'depends_on_past': True,
    'start_date': days_ago(1),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    # 'retries': 1,
    # 'retry_delay': timedelta(minutes=1),
}

for _business_client, business_conf in datasets_cfg.items():
    _storage_name = business_conf['storage_name']
    _container_name = business_conf['container_name']
    _blob_name = business_conf['blob_name']
    _cloud_storage_conn_id = business_conf['cloud_storage_conn_id']
    _ffr_conn_id = business_conf['ffr_conn_id']
    _ra_gateway_conn_id = business_conf['ra_gateway_conn_id']

    for location_conf in business_conf['locations']:
        _location_id = location_conf['id']
        _ra_building_conn_id = location_conf['ra_building_conn_id']
        _start_date = location_conf['start_date']
        _schedule_interval = location_conf['schedule_interval']

        for dataset_conf in location_conf['datasets']:
            _interaction_type = dataset_conf['interaction_type']
            _interaction_field = dataset_conf.get('interaction_field')
            _filename = dataset_conf['filename']
            _file_ext = dataset_conf['file_extension']

            _dag_id = f"load_file_{_business_client}_{_location_id}_{_interaction_type}_{_filename}"

            _dag = create_dag(
                dag_id=_dag_id,
                start_date=None,
                # parse_execution_date(_start_date),
                schedule_interval=_schedule_interval,
                default_args=_default_args,
                container_name=_container_name,
                cloud_storage_conn_id=_cloud_storage_conn_id,
                _ffr_conn_id=_ffr_conn_id,
                ra_gateway_conn_id=_ra_gateway_conn_id,
                business_client=_business_client,
                location_id=_location_id,
                ra_building_conn_id=_ra_building_conn_id,
                interaction_type=_interaction_type,
                interaction_field=_interaction_field,
                filename=_filename,
                file_ext=_file_ext
            )

            if _dag is not None:
                globals()[_dag_id] = _dag
