import json
import time
from tempfile import NamedTemporaryFile
import os
import pandas as pd
from airflow.hooks.http_hook import HttpHook
from airflow.utils.decorators import apply_defaults
from airflow.contrib.hooks.wasb_hook import WasbHook
from datetime import datetime, timedelta
from airflow.models import BaseOperator


class RetailAnalyticsVisitsToWasbOperator(BaseOperator):
    template_fields = (
        'ra_gateway_token', 'ra_building_conn_id', 'wasb_conn_id', 'tenant_map_wasb_obj', 'visits_wasb_obj')

    @apply_defaults
    def __init__(self,
                 ra_gateway_token: str,
                 ra_building_conn_id: str,
                 wasb_conn_id: str,
                 since: str,
                 until: str,
                 cursor_size=500,
                 tenant_map_wasb_obj=None,
                 visits_wasb_obj=None,
                 *args, **kwargs) -> None:

        super().__init__(*args, **kwargs)

        self.ra_gateway_token = ra_gateway_token
        self.ra_building_conn_id = ra_building_conn_id
        self.wasb_conn_id = wasb_conn_id
        self.since = since
        self.until = until
        self.subvisits = 'true'
        self.points = 'false'
        self.cursor_size = cursor_size
        self.tenant_map_wasb_obj = tenant_map_wasb_obj
        self.visits_wasb_obj = visits_wasb_obj

    def execute(self, context):
        global interaction
        self.log.info(f"RA Gateway token {self.ra_gateway_token}")
        self.log.info(f"Tenant map file {self.tenant_map_wasb_obj}")
        data = []
        wasb_hook = WasbHook(wasb_conn_id=self.wasb_conn_id)

        az_container_name = 'rs-dataset/nepirockcastle'
        az_blob_name = 'tenant_mappings.json'
        str_buff = wasb_hook.read_file(az_container_name, az_blob_name)

        tenant_map = json.loads(str_buff)
        tenant_map = {document['retailAnalyticsId']: (document['tenantId'], document['tenantName'])
                      for document in tenant_map}
        ra_building_hook = HttpHook(http_conn_id=self.ra_building_conn_id)
        # delete previous cursor and start fresh
        ra_building_hook.method = 'DELETE'
        delete_cursor_rsp = ra_building_hook.run(
            endpoint='/api/visits/cursor',
            headers={
                'Authorization': f"Bearer {self.ra_gateway_token}"
            }
        )

        ra_building_hook.method = 'GET'
        if delete_cursor_rsp:
            dc_rsp = delete_cursor_rsp.json()
            if dc_rsp['success'] is True:
                create_cursor_rsp = ra_building_hook.run(
                    endpoint='/api/visits/cursor/create',
                    data={
                        'since': self.since,
                        'until': self.until,
                        'subvisits': self.subvisits,
                        'points': self.points,
                        'cursor_size': self.cursor_size
                    },
                    headers={
                        'Authorization': f"Bearer {self.ra_gateway_token}"
                    }
                )
                if create_cursor_rsp:
                    cc_rsp = create_cursor_rsp.json()
                    if '_id' in cc_rsp:
                        has_next_cursor_info = True
                        while has_next_cursor_info:
                            next_cursor_rsp = ra_building_hook.run(
                                endpoint='/api/visits/cursor/next',
                                headers={
                                    'Authorization': f"Bearer {self.ra_gateway_token}"
                                }
                            )
                            if next_cursor_rsp:
                                nc_rsp = next_cursor_rsp.json()
                                has_next_cursor_info = nc_rsp['cursor']
                                visits = nc_rsp['visits']

                                # extract visits
                                for visit in visits:
                                    floor_level_visits = visit['subvisits']
                                    for floor_visit in floor_level_visits:
                                        for area_visit in floor_visit['subvisits']:
                                            area_id = area_visit['area']
                                            if area_id in tenant_map:
                                                if area_visit['duration'] == 0:
                                                    interaction = area_visit['duration'] + 1
                                                else:
                                                    interaction = area_visit['duration']
                                                data.append({
                                                    'visit_id': visit['_id'],
                                                    'start_ts': area_visit['start'],
                                                    'end_ts': area_visit['end'],
                                                    'tenant_id': tenant_map[area_id][0],
                                                    'tenant_name': tenant_map[area_id][1],
                                                    'dwell_time': interaction
                                                })

                            else:
                                has_next_cursor_info = False
                            time.sleep(0.3)

                        if data:
                            # create and pass the path
                            with NamedTemporaryFile(mode='w', delete=True) as f:
                                # json.dump(data, f)
                                pd.DataFrame(data).to_csv(f, index=False)
                                f.flush()
                                # upload file to destination
                                az_container_name = f'{self.visits_wasb_obj}'
                                az_blob_name = 'tenants.csv'
                                local_path = os.path.expanduser("/tmp/")
                                local_file_name = f.name
                                full_path_to_file = os.path.join(local_path, local_file_name)
                                wasb_hook.load_file(full_path_to_file, az_container_name, az_blob_name)

                # delete cursor
                ra_building_hook.method = 'DELETE'
                _ = ra_building_hook.run(
                    endpoint='/api/visits/cursor',
                    headers={
                        'Authorization': f"Bearer {self.ra_gateway_token}"
                    }
                )
        return self.visits_wasb_obj if data else None